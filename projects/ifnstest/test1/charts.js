var chartConfigs = {
	"chart-1": {
		chart: {
			type: 'donut',
			height: 400
		},
		series: [33.4, 67.6],
		labels: ["Инспекция", "Нал-щик"],
		colors: ['#6dc4de', '#2963aa'],
		dataLabels: {
			style: {
				colors: ['#000000']
			},
			dropShadow: {
				enabled: false
			}
		},
		title: {
			text: 'Досудебное урегулирование споров',
			align: 'center',
			style:{
				fontSize: '20px',
				color: '#466fa5'
			}
		},
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	},
	"chart-2": {
		chart: {
			height: 400,
			type: 'bar'
		},
		colors: ['#e4e9e9'],
		dataLabels: {
			enabled: false
		},
		fill:{
			opacity: 0.7
		},
		grid: {
		    show: true,
		},
		series: [{
			name: 'Debts',
			data: [45, 35, 31, 25, 22, 15]
		}],

		xaxis: {
			type: 'category',
			categories: ['05.02.18', '01.06.18', '03.01.18', '04.10.19', '10.11.19', '20.11.19'],
			position: 'bottom',
			axisBorder: {
				show: false
			},
			axisTicks: {
				show: false
			},
			crosshairs: {
				fill: {
					type: 'gradient',
					gradient: {
						colorFrom: '#D8E3F0',
						colorTo: '#BED1E6',
						stops: [0, 100],
						opacityFrom: 0.4,
						opacityTo: 0.5,
					}
				}
			},
			tooltip: {
				enabled: true,
				offsetY: -35,

			}
		},
		yaxis: {
			show: true,
			showAlways: true,
			seriesName: 'Debts',
			min: 0,
			max: 50,
			axisBorder: {
				show: false
			},
			axisTicks: {
				show: !false,
			},
			labels: {
				show: true,
				style:{
					//cssClass: 'apexcharts-yaxis-label'
				},
			}

		},
		title: {
			text: 'Сведения о задолженности',
			align: 'center',
			style: {
				fontSize: '20px',
				color: '#466fa5'
			}
		}
	},
	"chart-3": {
		chart: {
			type: 'area',
			height: 400,
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth'
		},
		series: [{
			name: 'series1',
			data: [25, 19, 35, 40, 32, 49]
		}],

		xaxis: {
			type: 'category',
			categories: ['05.02.18', '01.06.18', '03.01.18', '04.10.19', '10.11.19', '20.11.19'],
			position: 'bottom',
			//offsetY: -10
		},
		yaxis:{
			show: false,
			min: 0,
			max: 50
		},
		title: {
			text: 'Отчет по судебным делам',
			align: 'center',
			style: {
				fontSize: '20px',
				color: '#466fa5'
			}
		}
	},
	"chart-4": {
		chart: {
			type: 'radialBar',
			height: 400,
		},
		plotOptions: {
			radialBar: {
				hollow: {
					size: '70%',
				},
				startAngle: -90,
				endAngle: 90,
				track: {
					background: '#9dccdd',
					startAngle: -90,
					endAngle: 90,
				},
			},
		},
		fill: {
			colors: ['#3a639c']
		},
		series: [87.5],
		labels: ['ИПП'],
		stroke: {
			lineCap: 'round'
		},
		title: {
			text: 'Информация по мобилизации налогов',
			align: 'center',
			style: {
				fontSize: '20px',
				color: '#466fa5'
			}
		}
	}
}

var chart1 = new ApexCharts(document.querySelector("#chart-1"), chartConfigs['chart-1']),
	chart2 = new ApexCharts(document.querySelector("#chart-2"), chartConfigs['chart-2']),
	chart3 = new ApexCharts(document.querySelector("#chart-3"), chartConfigs['chart-3']),
	chart4 = new ApexCharts(document.querySelector("#chart-4"), chartConfigs['chart-4']);

chart1.render();
chart2.render();
chart3.render();
chart4.render();